﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTNLightningAlert.Core.Model
{
    public class AssetModel
    {
        public string AssetName { get; set; }
        public string QuadKey { get; set; }
        public string AssetOwner { get; set; }
    }
}
