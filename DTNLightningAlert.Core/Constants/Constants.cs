﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTNLightningAlert.Core.Constants
{
    public static class FlashTypeConstants
    {
        public const int CloudToGround = 0;
        public const int CloudToClound = 1;
        public const int HeartBeat = 9;
    }
}
